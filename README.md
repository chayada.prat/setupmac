# SetupMac
#### Create ssh keygen
```
ssh-keygen
```
output
```
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/chayada/.ssh/id_rsa): 
/Users/chayada/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /Users/chayada/.ssh/id_rsa
Your public key has been saved in /Users/chayada/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:oPN2294s2i4U2UpW+zKMr9MZ9kC3G4L7Zibwlg2OdMo chayada@Chayadas-MacBook-Pro.local
The key's randomart image is:
+---[RSA 3072]----+
|                 |
|          .      |
|      .  + .     |
|     . .= + .    |
|    o  oSB o .   |
|     oo B O +    |
|     ooO.O O o   |
|     .E.X+Xoo    |
|       .+&*.o    |
+----[SHA256]-----+
```
Get new public key and send to saved remote repository
#### Install brew
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
# works on mac m1
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> /Users/$USER/.zprofile
eval $(/opt/homebrew/bin/brew shellenv)
echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> /Users/$USER/.zprofile
eval $(/opt/homebrew/bin/brew shellenv)

# Check brew command "brew -h"
```
Output
```
Example usage:
  brew search TEXT|/REGEX/
  brew info [FORMULA|CASK...]
  brew install FORMULA|CASK...
  brew update
  brew upgrade [FORMULA|CASK...]
  brew uninstall FORMULA|CASK...
  brew list [FORMULA|CASK...]

Troubleshooting:
  brew config
  brew doctor
  brew install --verbose --debug FORMULA|CASK

Contributing:
  brew create URL [--no-fetch]
  brew edit [FORMULA|CASK...]

Further help:
  brew commands
  brew help [COMMAND]
  man brew
  https://docs.brew.sh
```
#### Install nodejs via brew command
```
brew install nodebrew
```
Output
```
# Check via command "node -v" and "npm -v"

node -v
v16.13.2

npm -v
8.1.2
```
#### Install newman as admin
```
sudo su
npm install -g newman

# Check newman version "newman -v"
```
Output
```
# newman -v
5.3.1
```